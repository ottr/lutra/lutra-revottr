package xyz.ottr.lutra.revottr.performance;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-revottr
 * %%
 * Copyright (C) 2018 - 2023 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;

public abstract class DataGenerator {

    public void writeInstancesToFile(String filePath, String prefixes, int numInstances) {
        try (OutputStream stream = new FileOutputStream(filePath);
             Writer writer = new OutputStreamWriter(stream, StandardCharsets.UTF_8)) {

            writer.write(prefixes);
            for (int i = 0; i < numInstances; i++) {
                writer.write(createInstance());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public abstract String getTemplateIRI();

    public abstract String getLibPath();

    public abstract String getGraphPath(int numInstances);

    protected abstract String createInstance();

    protected abstract void writeGraph(int numInstances);
}
