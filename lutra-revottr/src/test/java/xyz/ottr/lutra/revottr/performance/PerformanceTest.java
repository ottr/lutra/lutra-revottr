package xyz.ottr.lutra.revottr.performance;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-revottr
 * %%
 * Copyright (C) 2018 - 2023 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.time.Duration;
import java.time.Instant;
import java.util.Set;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import xyz.ottr.lutra.api.StandardFormat;
import xyz.ottr.lutra.revottr.evaluation.Evaluator;

@Disabled
public class PerformanceTest {

    private void speedTest(DataGenerator dataGenerator, int numInstances) {
        dataGenerator.writeGraph(numInstances);

        int iterations = 5;
        Duration totalDuration = Duration.ZERO;
        for (int i = 0; i < iterations; i++) {
            Instant timeBefore = Instant.now();

            Evaluator evaluator = new Evaluator(
                    dataGenerator.getGraphPath(numInstances),
                    StandardFormat.stottr.name(),
                    Set.of(dataGenerator.getLibPath()),
                    0
            );
            evaluator.evaluateQuery(dataGenerator.getTemplateIRI());

            Instant timeAfter = Instant.now();
            Duration duration = Duration.between(timeBefore, timeAfter);
            System.out.println(i + ">> Elapsed time: " + duration);
            totalDuration = totalDuration.plus(duration);
        }

        System.out.println("Average time: " + totalDuration.dividedBy(iterations));
    }

    private void memoryTest(DataGenerator dataGenerator, int numInstances) {
        dataGenerator.writeGraph(numInstances);

        Runtime runtime = Runtime.getRuntime();
        long usedMemoryBefore = runtime.totalMemory() - runtime.freeMemory();
        System.gc();
        long usedMemoryBeforeAfterGc = runtime.totalMemory() - runtime.freeMemory();
        System.out.println("Used memory before: " + usedMemoryBefore / 1000000 + " MB");
        System.out.println("Used memory before (after gc): " + usedMemoryBeforeAfterGc / 1000000 + " MB");

        Evaluator evaluator = new Evaluator(
                dataGenerator.getGraphPath(numInstances),
                StandardFormat.stottr.name(),
                Set.of(dataGenerator.getLibPath()),
                0
        );
        evaluator.evaluateQuery(dataGenerator.getTemplateIRI());

        long usedMemoryAfter = runtime.totalMemory() - runtime.freeMemory();
        System.gc();
        long usedMemoryAfterAfterGc = runtime.totalMemory() - runtime.freeMemory();
        System.out.println("Used memory after: " + usedMemoryAfter / 1000000 + " MB");
        System.out.println("Used memory after (after gc): " + usedMemoryAfterAfterGc / 1000000 + " MB");
    }

    @Test
    public void speedTest_100() {
        speedTest(new StudentDataGenerator(), 100);
    }

    @Test
    public void speedTest_1000() {
        speedTest(new StudentDataGenerator(), 1000);
    }

    @Test
    public void memoryTest_1000() {
        memoryTest(new StudentDataGenerator(), 1000);
    }
}
