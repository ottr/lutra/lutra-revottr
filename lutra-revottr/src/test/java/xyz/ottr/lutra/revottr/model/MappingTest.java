package xyz.ottr.lutra.revottr.model;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-revottr
 * %%
 * Copyright (C) 2018 - 2023 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static xyz.ottr.lutra.revottr.TestUtil.createTestVar;

import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.Test;
import xyz.ottr.lutra.model.terms.IRITerm;
import xyz.ottr.lutra.model.terms.ListTerm;
import xyz.ottr.lutra.model.terms.Term;

public class MappingTest {

    private final Mapping mapA = createTestMapping("x", "a");
    private final Mapping mapB = createTestMapping("y", "b");
    private final Mapping mapC = createTestMapping("z", "c");
    private final Mapping mapD = createTestMapping("z", "d");

    private Mapping createTestMapping(String varID, String valueID) {
        return new Mapping(createTestVar(varID), new IRITerm(valueID));
    }

    @Test
    public void joinTest1() {
        Set<Mapping> shouldEqual =
                Set.of(new Mapping(
                        List.of(createTestVar("x"),
                                createTestVar("y")),
                        List.of(new IRITerm("a"),
                                new IRITerm("b"))));

        Set<Mapping> setA = Set.of(mapA);
        Set<Mapping> setB = Set.of(mapB);

        assertEquals(Mappings.join(setA, setB), shouldEqual);
    }

    @Test
    public void joinTest2() {
        Set<Mapping> setC = Set.of(mapC);
        Set<Mapping> setD = Set.of(mapD);

        assertEquals(Mappings.join(setC, setD), Set.of());
    }

    @Test
    public void joinAllTest() {
        Set<Mapping> shouldEqual =
                Set.of(new Mapping(
                        List.of(createTestVar("x"),
                                createTestVar("y"),
                                createTestVar("z")),
                        List.of(new IRITerm("a"),
                                new IRITerm("b"),
                                new IRITerm("c"))));

        Set<Set<Mapping>> set = Set.of(
                Set.of(mapA),
                Set.of(mapB),
                Set.of(mapC));

        assertEquals(Mappings.joinAll(set), shouldEqual);
    }

    @Test
    public void transformTest1() {
        Term v1 = createTestVar("x");
        Term v2 = createTestVar("y");
        Term value = new IRITerm("a");

        Mapping argMap = new Mapping(v1, v2);
        Mapping termMap = new Mapping(v1, value);
        Mapping shouldEqual = new Mapping(v2, value);

        assertEquals(Mappings.transform(termMap, argMap), shouldEqual);
    }

    @Test
    public void transformTest2() {
        Term x1 = createTestVar("x1");
        Term x2 = createTestVar("x2");
        Term y1 = createTestVar("y1");
        Term value1 = new IRITerm("a");
        Term value2 = new IRITerm("b");

        Mapping argMap = new Mapping(List.of(x1, x2), List.of(value1, y1));
        Mapping termMap = new Mapping(List.of(x1, x2), List.of(value1, value2));
        Mapping shouldEqual = new Mapping(y1, value2);

        assertEquals(Mappings.transform(termMap, argMap), shouldEqual);
    }

    @Test
    public void transformListTest1() {
        Term x1 = createTestVar("x1");
        Term y1 = createTestVar("y1");
        Term e1 = new IRITerm("a");
        Term e2 = new IRITerm("b");
        Term value1 = new ListTerm(List.of(e1, y1));
        Term value2 = new ListTerm(List.of(e1, e2));

        Mapping argMap = new Mapping(x1, value1);
        Mapping termMap = new Mapping(x1, value2);
        Mapping shouldEqual = new Mapping(y1, e2);

        assertEquals(Mappings.transform(termMap, argMap), shouldEqual);
    }

    @Test
    public void innerCompatibleTest1() {
        Term x1 = createTestVar("x1");
        Term x2 = createTestVar("x2");
        Term y1 = createTestVar("y1");
        Term value1 = new IRITerm("a");
        Term value2 = new IRITerm("b");

        Mapping argMap = new Mapping(List.of(x1, x2), List.of(value2, y1));
        Mapping termMap = new Mapping(List.of(x1, x2), List.of(value1, value2));

        assertFalse(Mappings.innerCompatible(termMap, argMap));
    }

    @Test
    public void innerCompatibleTest2() {
        Term x1 = createTestVar("x1");
        Term x2 = createTestVar("x2");
        Term y1 = createTestVar("y1");
        Term value1 = new IRITerm("a");
        Term value2 = new IRITerm("b");

        Mapping argMap = new Mapping(List.of(x1, x2), List.of(value1, y1));
        Mapping termMap = new Mapping(List.of(x1, x2), List.of(value1, value2));

        assertTrue(Mappings.innerCompatible(termMap, argMap));
    }
}
