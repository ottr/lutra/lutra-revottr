package xyz.ottr.lutra.revottr.performance;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-revottr
 * %%
 * Copyright (C) 2018 - 2023 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;
import xyz.ottr.lutra.cli.CLI;

public class StudentDataGenerator extends DataGenerator {

    private final Path root = Paths
            .get("src", "test", "resources", "performance")
            .toAbsolutePath();

    private final String libPath = root.resolve("student_lib.stottr").toString();

    private final Random random = new Random(2023);

    @Override
    public String getLibPath() {
        return libPath;
    }

    @Override
    public String getTemplateIRI() {
        return "http://example.com/ns#Student";
    }

    @Override
    public String getGraphPath(int numInstances) {
        String graphFile = "graph_" + numInstances + ".ttl";
        return root.resolve(graphFile).toString();
    }

    @Override
    protected String createInstance() {
        int num = Math.abs(random.nextInt());
        return "ex:Student(_:person" + num
                + ", \"John" + num
                + "\", " + num
                + ", _:university" + num + ") .\n";
    }

    @Override
    protected void writeGraph(int numInstances) {
        String instanceFile = "instances_" + numInstances + ".stottr";
        String instancePath = root.resolve(instanceFile).toString();

        String prefix = "@prefix ex: <http://example.com/ns#> .\n"
                + "@prefix ottr: <http://ns.ottr.xyz/0.4/> .\n";

        if (!(new File(instancePath).exists())) {
            writeInstancesToFile(instancePath, prefix, numInstances);
        }

        String graphPath = getGraphPath(numInstances);
        String args = "-l " + getLibPath()
                + " -L stottr -I stottr "
                + instancePath + " -o "
                + graphPath;

        if (!(new File(graphPath).exists())) {
            new CLI().executeArgs(args.split("\\s+"));
        }
    }
}
