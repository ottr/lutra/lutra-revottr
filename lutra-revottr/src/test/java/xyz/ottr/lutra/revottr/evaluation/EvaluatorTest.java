package xyz.ottr.lutra.revottr.evaluation;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-revottr
 * %%
 * Copyright (C) 2018 - 2023 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.jena.rdf.model.ModelFactory;
import org.junit.jupiter.api.Test;
import xyz.ottr.lutra.OTTR;
import xyz.ottr.lutra.RDFTurtle;
import xyz.ottr.lutra.TemplateManager;
import xyz.ottr.lutra.api.StandardFormat;
import xyz.ottr.lutra.api.StandardTemplateManager;
import xyz.ottr.lutra.model.Parameter;
import xyz.ottr.lutra.model.terms.IRITerm;
import xyz.ottr.lutra.model.terms.LiteralTerm;
import xyz.ottr.lutra.model.terms.NoneTerm;
import xyz.ottr.lutra.model.terms.Term;
import xyz.ottr.lutra.revottr.TestUtil;
import xyz.ottr.lutra.revottr.model.Mapping;
import xyz.ottr.lutra.revottr.model.terms.Placeholder;
import xyz.ottr.lutra.revottr.model.terms.RListTerm;

public class EvaluatorTest {

    private final String baseIRI = "http://example.org/example/";

    private final String graphPath = TestUtil.ROOT.resolve("testGraph.ttl").toString();
    private final String libPath = TestUtil.ROOT.resolve("testLib.stottr").toString();

    private final StandardTemplateManager templateManager = createTemplateManager(libPath);

    private final Evaluator evaluator = new Evaluator(
            ModelFactory.createDefaultModel().read(graphPath),
            templateManager, 0
    );

    private StandardTemplateManager createTemplateManager(String libPath) {
        StandardTemplateManager templateManager = new StandardTemplateManager();
        templateManager.readLibrary(StandardFormat.stottr.name(), Collections.singleton(libPath));
        templateManager.loadStandardTemplateLibrary();
        return templateManager;
    }

    private List<Term> getParamVars(TemplateManager templateManager, String queryIRI) {
        return templateManager.getTemplateStore()
                .getSignature(queryIRI).get()
                .getParameters().stream().map(Parameter::getTerm)
                .collect(Collectors.toList());
    }

    @Test
    public void evaluateQueryTest1() {
        String queryIRI = baseIRI + "Person";
        List<Term> paramVars = getParamVars(templateManager, queryIRI);

        Term johnName = LiteralTerm.createTypedLiteral("John", RDFTurtle.plainLiteralDatatype);
        Term johnAge = LiteralTerm.createTypedLiteral("41", OTTR.TypeURI.Integer);

        Term janeName = LiteralTerm.createTypedLiteral("Jane", RDFTurtle.plainLiteralDatatype);
        Term janeAge = LiteralTerm.createTypedLiteral("42", OTTR.TypeURI.Integer);

        Term judeName = LiteralTerm.createTypedLiteral("Jude", RDFTurtle.plainLiteralDatatype);
        Term judeAge = LiteralTerm.createTypedLiteral("27", OTTR.TypeURI.Integer);

        Set<Mapping> shouldEqual = Set.of(
                new Mapping(paramVars, List.of(
                        new NoneTerm(),
                        Placeholder.any,
                        Placeholder.any
                )),
                new Mapping(paramVars, List.of(
                        Placeholder.any,
                        new NoneTerm(),
                        Placeholder.any
                )),
                new Mapping(paramVars, List.of(
                        Placeholder.any,
                        Placeholder.any,
                        new NoneTerm()
                )),
                new Mapping(paramVars, List.of(
                        new IRITerm(baseIRI + "john"),
                        johnName,
                        johnAge
                )),
                new Mapping(paramVars, List.of(
                        new IRITerm(baseIRI + "jane"),
                        janeName,
                        janeAge
                )),
                new Mapping(paramVars, List.of(
                        new IRITerm(baseIRI + "jude"),
                        judeName,
                        judeAge
                ))
        );

        Set<Mapping> result = evaluator.evaluateQuery(queryIRI);
        assertEquals(shouldEqual, result);
    }

    @Test
    public void evaluateQueryNestingTest() {
        String queryIRI = baseIRI + "UiOStudent";
        List<Term> paramVars = getParamVars(templateManager, queryIRI);

        Term johnName = LiteralTerm.createTypedLiteral("John", RDFTurtle.plainLiteralDatatype);
        Term johnAge = LiteralTerm.createTypedLiteral("41", OTTR.TypeURI.Integer);

        Term janeName = LiteralTerm.createTypedLiteral("Jane", RDFTurtle.plainLiteralDatatype);
        Term janeAge = LiteralTerm.createTypedLiteral("42", OTTR.TypeURI.Integer);

        Set<Mapping> shouldEqual = Set.of(
                new Mapping(paramVars, List.of(
                        new NoneTerm(),
                        Placeholder.any,
                        Placeholder.any
                )),
                new Mapping(paramVars, List.of(
                        Placeholder.any,
                        new NoneTerm(),
                        Placeholder.any
                )),
                new Mapping(paramVars, List.of(
                        Placeholder.any,
                        Placeholder.any,
                        new NoneTerm()
                )),
                new Mapping(paramVars, List.of(
                        new IRITerm(baseIRI + "john"),
                        johnName,
                        johnAge
                )),
                new Mapping(paramVars, List.of(
                        new IRITerm(baseIRI + "jane"),
                        janeName,
                        janeAge
                ))
        );

        Set<Mapping> result = evaluator.evaluateQuery(queryIRI);
        assertEquals(shouldEqual, result);
    }

    @Test
    public void evaluateQueryOptionalTest() {
        String queryIRI = baseIRI + "Course";
        List<Term> paramVars = getParamVars(templateManager, queryIRI);

        Term semwebName = LiteralTerm.createTypedLiteral("Semantic Web", RDFTurtle.plainLiteralDatatype);
        Term semwebCode = LiteralTerm.createTypedLiteral("1", RDFTurtle.plainLiteralDatatype);

        Term logicName = LiteralTerm.createTypedLiteral("Logic", RDFTurtle.plainLiteralDatatype);
        Term logicCode = LiteralTerm.createTypedLiteral("2", RDFTurtle.plainLiteralDatatype);

        Term progName = LiteralTerm.createTypedLiteral("Programming", RDFTurtle.plainLiteralDatatype);
        Term progCode = LiteralTerm.createTypedLiteral("3", RDFTurtle.plainLiteralDatatype);

        Term osName = LiteralTerm.createTypedLiteral("Operating Systems", RDFTurtle.plainLiteralDatatype);
        Term osCode = LiteralTerm.createTypedLiteral("4", RDFTurtle.plainLiteralDatatype);

        Term written = new IRITerm(baseIRI + "writtenExam");
        Term oral = new IRITerm(baseIRI + "oralExam");

        Set<Mapping> shouldEqual = Set.of(
                new Mapping(paramVars, List.of(
                        new NoneTerm(),
                        Placeholder.any,
                        Placeholder.any,
                        Placeholder.any
                )),
                new Mapping(paramVars, List.of(
                        Placeholder.any,
                        new NoneTerm(),
                        Placeholder.any,
                        Placeholder.any
                )),
                new Mapping(paramVars, List.of(
                        Placeholder.any,
                        Placeholder.any,
                        new NoneTerm(),
                        Placeholder.any
                )),
                new Mapping(paramVars, List.of(
                        new IRITerm(baseIRI + "semanticWeb"),
                        semwebName,
                        semwebCode,
                        oral
                )),
                new Mapping(paramVars, List.of(
                        new IRITerm(baseIRI + "logic"),
                        logicName,
                        logicCode,
                        written
                )),
                new Mapping(paramVars, List.of(
                        new IRITerm(baseIRI + "programming"),
                        progName,
                        progCode,
                        written
                )),
                new Mapping(paramVars, List.of(
                        new IRITerm(baseIRI + "semanticWeb"),
                        semwebName,
                        semwebCode,
                        new NoneTerm()
                )),
                new Mapping(paramVars, List.of(
                        new IRITerm(baseIRI + "logic"),
                        logicName,
                        logicCode,
                        new NoneTerm()
                )),
                new Mapping(paramVars, List.of(
                        new IRITerm(baseIRI + "programming"),
                        progName,
                        progCode,
                        new NoneTerm()
                )),
                new Mapping(paramVars, List.of(
                        new IRITerm(baseIRI + "operatingSystems"),
                        osName,
                        osCode,
                        new NoneTerm()
                ))
        );

        Set<Mapping> result = evaluator.evaluateQuery(queryIRI);
        assertEquals(shouldEqual, result);
    }

    @Test
    public void evaluateQueryListExpansionTest() {
        String queryIRI = baseIRI + "TakesCourses";
        List<Term> paramVars = getParamVars(templateManager, queryIRI);

        Set<Mapping> shouldContain = Set.of(
                new Mapping(paramVars, List.of(
                        new NoneTerm(),
                        Placeholder.any
                )),
                new Mapping(paramVars, List.of(
                        Placeholder.any,
                        new NoneTerm()
                )),
                new Mapping(paramVars, List.of(
                        Placeholder.anyNonOptional,
                        new RListTerm(List.of())
                )),
                new Mapping(paramVars, List.of(
                        new IRITerm(baseIRI + "john"),
                        new RListTerm(List.of(
                                new IRITerm(baseIRI + "semanticWeb"),
                                new IRITerm(baseIRI + "programming"),
                                new IRITerm(baseIRI + "logic")
                        ))
                )),
                new Mapping(paramVars, List.of(
                        new IRITerm(baseIRI + "jane"),
                        new RListTerm(List.of(
                                new IRITerm(baseIRI + "semanticWeb"),
                                new IRITerm(baseIRI + "logic"),
                                new IRITerm(baseIRI + "operatingSystems")
                        ))
                ))
        );

        Set<Mapping> result = evaluator.evaluateQuery(queryIRI);
        for (Mapping mapping : shouldContain) {
            assertTrue(result.contains(mapping));
        }
    }

    // TODO: add test for default values
}
