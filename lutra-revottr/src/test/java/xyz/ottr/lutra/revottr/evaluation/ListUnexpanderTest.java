package xyz.ottr.lutra.revottr.evaluation;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-revottr
 * %%
 * Copyright (C) 2018 - 2023 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static xyz.ottr.lutra.revottr.TestUtil.createTestVar;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;
import xyz.ottr.lutra.model.Argument;
import xyz.ottr.lutra.model.Parameter;
import xyz.ottr.lutra.model.terms.IRITerm;
import xyz.ottr.lutra.model.terms.Term;
import xyz.ottr.lutra.revottr.TestUtil;
import xyz.ottr.lutra.revottr.model.Mapping;
import xyz.ottr.lutra.revottr.model.terms.Placeholder;
import xyz.ottr.lutra.revottr.model.terms.RListTerm;

public class ListUnexpanderTest {

    private final List<String> paramLabels = List.of("x1", "x2", "x3");

    private final List<Term> paramVars = paramLabels.stream()
            .map(TestUtil::createTestVar)
            .collect(Collectors.toList());

    private final List<Parameter> parameters = paramVars.stream()
            .map(x -> Parameter.builder().term(x).build())
            .collect(Collectors.toList());

    private final List<Argument> arguments = List.of(
            Argument.builder().term(createTestVar("y1")).listExpander(true).build(),
            Argument.builder().term(new IRITerm("b")).build(),
            Argument.builder().term(createTestVar("y2")).listExpander(true).build()
    );

    private final Mapping minEmpty1 = new Mapping(paramVars, List.of(
            new RListTerm(List.of(Placeholder.any_trail)),
            Placeholder.any,
            new RListTerm(List.of())
    ));

    private final Mapping minEmpty2 = new Mapping(paramVars, List.of(
            new RListTerm(List.of()),
            Placeholder.any,
            new RListTerm(List.of(Placeholder.any_trail))
    ));

    private final Mapping maxEmpty = new Mapping(paramVars, List.of(
            new RListTerm(List.of()),
            Placeholder.any,
            new RListTerm(List.of())
    ));

    private final ListUnexpander listUnexpander = new ListUnexpander(parameters, arguments, 0);

    private final Set<Mapping> input = Set.of(
            new Mapping(paramVars, createIRITermList(List.of("a1", "b", "c1"))),
            new Mapping(paramVars, createIRITermList(List.of("a1", "b", "c2")))
    );

    private final List<List<Term>> unzipFirstLists = List.of(
            createIRITermList(List.of("a1")),
            createIRITermList(List.of("a1")),
            createIRITermList(List.of("a1", "a1")),
            createIRITermList(List.of("a1", "a1"))
    );

    private final List<List<Term>> unzipSecondLists = List.of(
            createIRITermList(List.of("c1")),
            createIRITermList(List.of("c2")),
            createIRITermList(List.of("c1", "c2")),
            createIRITermList(List.of("c2", "c1"))
    );

    private final List<List<Term>> uncrossFirstLists = List.of(
            createIRITermList(List.of("a1")),
            createIRITermList(List.of("a1")),
            createIRITermList(List.of("a1")),
            createIRITermList(List.of("a1"))
    );

    private final List<List<Term>> uncrossSecondLists = List.of(
            createIRITermList(List.of("c1")),
            createIRITermList(List.of("c2")),
            createIRITermList(List.of("c1", "c2")),
            createIRITermList(List.of("c2", "c1"))
    );

    private List<Term> createIRITermList(List<String> labels) {
        return labels.stream()
                .map(IRITerm::new)
                .collect(Collectors.toList());
    }

    @Test
    public void unzipMinTest1() {

        Set<Mapping> shouldEqual = new HashSet<>();

        for (int i = 0; i < unzipFirstLists.size(); i++) {
            List<Term> firstExtended = new LinkedList<>(unzipFirstLists.get(i));
            firstExtended.add(Placeholder.any_trail);

            List<Term> secondExtended = new LinkedList<>(unzipSecondLists.get(i));
            secondExtended.add(Placeholder.any_trail);

            shouldEqual.add(new Mapping(paramVars, List.of(
                    new RListTerm(firstExtended),
                    new IRITerm("b"),
                    new RListTerm(unzipSecondLists.get(i))
            )));

            shouldEqual.add(new Mapping(paramVars, List.of(
                    new RListTerm(unzipFirstLists.get(i)),
                    new IRITerm("b"),
                    new RListTerm(secondExtended)
            )));
        }
        shouldEqual.add(minEmpty1);
        shouldEqual.add(minEmpty2);

        Set<Mapping> result = listUnexpander.unzipMin(input);

        assertEquals(result, shouldEqual);
    }

    @Test
    public void unzipMinEmptyTest() {
        Set<Mapping> shouldEqual = Set.of(minEmpty1, minEmpty2);
        Set<Mapping> result = listUnexpander.unzipMin(new HashSet<>());
        assertEquals(result, shouldEqual);
    }

    @Test
    public void unzipMaxTest1() {
        Set<Mapping> shouldEqual = new HashSet<>();
        for (int i = 0; i < unzipFirstLists.size(); i++) {
            shouldEqual.add(new Mapping(paramVars, List.of(
                    new RListTerm(unzipFirstLists.get(i)),
                    new IRITerm("b"),
                    new RListTerm(unzipSecondLists.get(i))
            )));
        }
        shouldEqual.add(maxEmpty);
        Set<Mapping> result = listUnexpander.unzipMax(input);
        assertEquals(result, shouldEqual);
    }

    // TODO: test unzipMax with trailing NoneTerms

    @Test
    public void unzipMaxEmptyTest() {
        Set<Mapping> shouldEqual = Set.of(maxEmpty);
        Set<Mapping> result = listUnexpander.unzipMax(new HashSet<>());
        assertEquals(result, shouldEqual);
    }

    @Test
    public void uncrossTest1() {
        Set<Mapping> shouldEqual = new HashSet<>();

        for (int i = 0; i < uncrossFirstLists.size(); i++) {
            shouldEqual.add(new Mapping(paramVars, List.of(
                    new RListTerm(uncrossFirstLists.get(i)),
                    new IRITerm("b"),
                    new RListTerm(uncrossSecondLists.get(i))
            )));
        }

        shouldEqual.add(minEmpty1);
        shouldEqual.add(minEmpty2);

        Set<Mapping> result = listUnexpander.uncross(input);

        assertEquals(result, shouldEqual);
    }

    @Test
    public void uncrossEmptyTest() {
        Set<Mapping> shouldEqual = Set.of(minEmpty1, minEmpty2);
        Set<Mapping> result = listUnexpander.uncross(new HashSet<>());
        assertEquals(result, shouldEqual);
    }
}
