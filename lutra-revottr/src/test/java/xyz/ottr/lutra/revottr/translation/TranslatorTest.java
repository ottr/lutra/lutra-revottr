package xyz.ottr.lutra.revottr.translation;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-revottr
 * %%
 * Copyright (C) 2018 - 2023 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;
import org.apache.commons.io.IOUtils;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.sparql.resultset.ResultSetCompare;
import org.junit.jupiter.api.Test;
import xyz.ottr.lutra.TemplateManager;
import xyz.ottr.lutra.api.StandardFormat;
import xyz.ottr.lutra.api.StandardTemplateManager;
import xyz.ottr.lutra.revottr.translation.model.SelectQuery;
import xyz.ottr.lutra.store.Expander;
import xyz.ottr.lutra.store.expansion.CheckingExpander;

public class TranslatorTest {

    private Path root = Paths.get("src", "test", "resources", "translation");
    private String libPath = root.resolve("translationTestLib.stottr").toAbsolutePath().toString();
    private String graphPath = root.resolve("translationTestGraph.ttl").toAbsolutePath().toString();

    private Translator translator = initTranslator();

    private Translator initTranslator() {
        TemplateManager templateManager = new StandardTemplateManager();
        templateManager.readLibrary(StandardFormat.stottr.name(), Set.of(libPath));
        Expander expander = new CheckingExpander(templateManager.getTemplateStore());
        return new Translator(templateManager, expander);
    }

    @Test
    public void notTranslatableTest1() {
        String templateIRI = "http://example.org/example/NotTranslatable1";
        SelectQuery result = translator.checkAndTranslate(templateIRI);
        assertNull(result);
    }

    @Test
    public void notTranslatableTest2() {
        String templateIRI = "http://example.org/example/NotTranslatable2";
        SelectQuery result = translator.checkAndTranslate(templateIRI);
        assertNull(result);
    }

    @Test
    public void translatableTest1() {
        String templateIRI = "http://example.org/example/Translatable1";
        SelectQuery result = translator.checkAndTranslate(templateIRI);
        assertNotNull(result);
    }

    @Test
    public void translatableTest2() {
        String templateIRI = "http://example.org/example/Translatable2";
        SelectQuery result = translator.checkAndTranslate(templateIRI);
        assertNotNull(result);
    }

    @Test
    public void translatableTest3() {
        String templateIRI = "http://example.org/example/Translatable3";
        SelectQuery result = translator.checkAndTranslate(templateIRI);
        assertNotNull(result);
    }

    @Test
    public void queryResultTest() {
        SelectQuery query = translator
                .checkAndTranslate("http://example.org/example/Translatable1");
        String sparqlQueryString = query.toString();
        Query jenaQuery = QueryFactory.create(sparqlQueryString);

        Model model = ModelFactory.createDefaultModel().read(graphPath);

        ResultSet result;
        try (QueryExecution execution = QueryExecutionFactory.create(jenaQuery, model)) {
            result = execution.execSelect().materialise();
        }

        String expectedJson = "{\"head\" : {\"vars\" : [ \"x\", \"y\" ]},"
                + "\"results\" : { \"bindings\" : ["
                + "{"
                + "\"x\" : {\"type\" : \"uri\" , \"value\" : \"http://example.org/example/a1\"},"
                + "\"y\" : {\"type\" : \"uri\" , \"value\" : \"http://example.org/example/a2\"}"
                + "},"
                + "{"
                + "\"x\" : {\"type\" : \"uri\" , \"value\" : \"http://example.org/example/b1\"},"
                + "\"y\" : {\"type\" : \"uri\" , \"value\" : \"http://example.org/example/b2\"}"
                + "},"
                + "{"
                + "\"x\" : {\"type\" : \"uri\" , \"value\" : \"http://example.org/example/c1\"}"
                + "}"
                + "]}}";

        InputStream inputStream = IOUtils.toInputStream(expectedJson, StandardCharsets.UTF_8);
        ResultSet expected = ResultSetFactory.fromJSON(inputStream);

        assertTrue(ResultSetCompare.equalsByValue(expected, result));
    }
}
