package xyz.ottr.lutra.revottr.model;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-revottr
 * %%
 * Copyright (C) 2018 - 2023 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import org.junit.jupiter.api.Test;
import xyz.ottr.lutra.model.terms.BlankNodeTerm;
import xyz.ottr.lutra.model.terms.IRITerm;
import xyz.ottr.lutra.model.terms.ListTerm;
import xyz.ottr.lutra.model.terms.NoneTerm;
import xyz.ottr.lutra.revottr.model.terms.Placeholder;
import xyz.ottr.lutra.revottr.model.terms.RListTerm;

public class PartialOrderTest {

    private final ListTerm listTermA = new RListTerm(List.of(
            new IRITerm("1"),
            Placeholder.anyNonBlankNonOptional,
            Placeholder.any_trail));

    private final ListTerm listTermB = new ListTerm(List.of(
            new IRITerm("1"),
            new IRITerm("2")));

    private final ListTerm listTermC = new RListTerm(List.of(
            new IRITerm("1"),
            new IRITerm("2"),
            Placeholder.any_trail));

    private final ListTerm listTermD = new RListTerm(List.of(
            Placeholder.anyNonBlankNonOptional,
            new IRITerm("2"),
            Placeholder.any_trail));

    @Test
    public void lessOrEqualTestBasic1() {
        assertTrue(PartialOrder.isLessOrEqual(Placeholder.anyNonBlank, Placeholder.any));
        assertTrue(PartialOrder.isLessOrEqual(Placeholder.anyNonOptional, Placeholder.any));
    }

    @Test
    public void lessOrEqualTestBasic2() {
        assertFalse(PartialOrder.isLessOrEqual(Placeholder.anyNonOptional, Placeholder.anyNonBlank));
        assertFalse(PartialOrder.isLessOrEqual(Placeholder.anyNonBlank, Placeholder.anyNonOptional));
        assertTrue(PartialOrder.isLessOrEqual(Placeholder.anyNonBlankNonOptional, Placeholder.anyNonBlank));
        assertTrue(PartialOrder.isLessOrEqual(Placeholder.anyNonBlankNonOptional, Placeholder.anyNonOptional));
    }

    @Test
    public void lessOrEqualTestBasic3() {
        assertFalse(PartialOrder.isLessOrEqual(new NoneTerm(), Placeholder.anyNonOptional));
        assertFalse(PartialOrder.isLessOrEqual(new NoneTerm(), Placeholder.anyNonBlankNonOptional));
        assertFalse(PartialOrder.isLessOrEqual(new BlankNodeTerm(), Placeholder.anyNonBlank));
        assertFalse(PartialOrder.isLessOrEqual(new BlankNodeTerm(), Placeholder.anyNonBlankNonOptional));
    }

    @Test
    public void lessOrEqualTestList1() {
        assertTrue(PartialOrder.isLessOrEqual(listTermB, listTermB));
    }

    @Test
    public void lessOrEqualTestList2() {
        assertTrue(PartialOrder.isLessOrEqual(listTermB, listTermA));
    }

    @Test
    public void lessOrEqualTestList3() {
        assertFalse(PartialOrder.isLessOrEqual(listTermA, listTermB));
    }

    @Test
    public void lessOrEqualTestList4() {
        assertTrue(PartialOrder.isLessOrEqual(listTermC, listTermA));
    }

    @Test
    public void glbTest1() {
        assertEquals(PartialOrder.getGLB(listTermC, listTermA), listTermC);
    }

    @Test
    public void glbTest2() {
        assertEquals(PartialOrder.getGLB(listTermB, listTermC), listTermB);
    }

    @Test
    public void glbTest3() {
        assertEquals(PartialOrder.getGLB(listTermA, listTermD), listTermC);
    }
}
