package xyz.ottr.lutra.revottr;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-revottr
 * %%
 * Copyright (C) 2018 - 2023 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import xyz.ottr.lutra.model.Argument;
import xyz.ottr.lutra.model.Instance;
import xyz.ottr.lutra.model.Parameter;
import xyz.ottr.lutra.model.Signature;
import xyz.ottr.lutra.model.terms.Term;
import xyz.ottr.lutra.revottr.model.Mapping;

public class Converter {

    /**
     * Converts the given set of mappings into instances of the given signature
     * (without expansion modes). It's assumed that the domain of the mappings
     * is equal to the set of parameter variables in the given signature.
     * @param mappings A set of mappings from template variables to terms
     * @param signature A template signature
     * @return A set of instances of the given signature. Each argument of each instance
     * is equal to the value mapped to by the corresponding parameter in a given mapping
     */
    public static Set<Instance> toInstances(Set<Mapping> mappings, Signature signature) {
        return mappings.stream()
                .map(m -> toInstance(m, signature))
                .collect(Collectors.toSet());
    }

    private static Instance toInstance(Mapping mapping, Signature signature) {
        List<Term> paramVars = signature.getParameters().stream()
                .map(Parameter::getTerm)
                .collect(Collectors.toList());

        List<Argument> arguments = paramVars.stream()
                .map(mapping::get)
                .map(Converter::toArgument)
                .collect(Collectors.toList());

        return Instance.builder()
                .iri(signature.getIri())
                .arguments(arguments)
                .build();
    }

    private static Argument toArgument(Term term) {
        return Argument.builder().term(term).build();
    }
}
