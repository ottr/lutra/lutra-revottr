package xyz.ottr.lutra.revottr.translation.model.pattern;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-revottr
 * %%
 * Copyright (C) 2018 - 2023 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.List;
import org.apache.jena.shared.PrefixMapping;
import xyz.ottr.lutra.revottr.translation.model.terms.QTerm;

public class TriplePattern extends Pattern {
    private QTerm subject;
    private QTerm predicate;
    private QTerm object;

    public TriplePattern(QTerm subject, QTerm predicate, QTerm object) {
        this.subject = subject;
        this.predicate = predicate;
        this.object = object;
    }

    public TriplePattern(List<QTerm> terms) {
        this(terms.get(0), terms.get(1), terms.get(2));
    }

    public String toString(PrefixMapping prefixMapping) {
        return subject.toString(prefixMapping) + " "
                + predicate.toString(prefixMapping) + " "
                + object.toString(prefixMapping) + " .\n";
    }
}
