package xyz.ottr.lutra.revottr.translation.model;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-revottr
 * %%
 * Copyright (C) 2018 - 2023 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.List;
import java.util.Map;
import org.apache.jena.shared.PrefixMapping;
import xyz.ottr.lutra.revottr.translation.model.pattern.Pattern;
import xyz.ottr.lutra.revottr.translation.model.terms.QTerm;
import xyz.ottr.lutra.revottr.translation.model.terms.Variable;

/**
 * Represents a SPARQL select query
 */
public class SelectQuery {
    private PrefixMapping prefixMapping;
    private List<Variable> variables;
    private Pattern pattern;

    public SelectQuery(PrefixMapping prefixMapping, List<Variable> variables, Pattern pattern) {
        this.prefixMapping = prefixMapping;
        this.variables = variables;
        this.pattern = pattern;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(prefixesToString());
        stringBuilder.append("select ");
        for (QTerm variable : variables) {
            stringBuilder.append(variable.toString(prefixMapping))
                    .append(" ");
        }

        stringBuilder.append("where {\n")
                .append(pattern.toString(prefixMapping))
                .append("}");
        return stringBuilder.toString();
    }

    private String prefixesToString() {
        StringBuilder stringBuilder = new StringBuilder();

        for (Map.Entry<String, String> entry : prefixMapping.getNsPrefixMap().entrySet()) {
            stringBuilder.append("prefix ")
                    .append(entry.getKey())
                    .append(": <")
                    .append(entry.getValue())
                    .append(">\n");
        }

        return stringBuilder.toString();
    }
}
