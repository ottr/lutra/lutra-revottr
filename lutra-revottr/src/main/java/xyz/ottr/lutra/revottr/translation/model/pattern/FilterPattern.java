package xyz.ottr.lutra.revottr.translation.model.pattern;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-revottr
 * %%
 * Copyright (C) 2018 - 2023 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import org.apache.jena.shared.PrefixMapping;
import xyz.ottr.lutra.revottr.translation.model.condition.Condition;
import xyz.ottr.lutra.revottr.translation.model.condition.EmptyCondition;

public class FilterPattern extends Pattern {
    private Pattern pattern;
    private Condition condition;

    public FilterPattern(Pattern pattern, Condition condition) {
        this.pattern = pattern;
        this.condition = condition;
    }

    public String toString(PrefixMapping prefixMapping) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(pattern.toString(prefixMapping));

        if (!(condition instanceof EmptyCondition)) {
            stringBuilder.append("filter(")
                    .append(condition.toString(prefixMapping))
                    .append(")");
        }

        return stringBuilder.toString();
    }
}
