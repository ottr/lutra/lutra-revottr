/**
 * Implements Reverse OTTR: A query language for RDF with OTTR syntax.
 * <p>
 * Given a template T, and an RDF graph G, Reverse OTTR finds all instances of T such that
 * expanding them with OTTR produces a subset of G. The process of finding these instances
 * can be thought of as the reverse of the expansion process of OTTR.
 * <p>
 * The main entry point of this module is the
 * {@link xyz.ottr.lutra.revottr.evaluation.Evaluator Evaluator} class and its
 * {@link xyz.ottr.lutra.revottr.evaluation.Evaluator#evaluateQuery(java.lang.String) evaluateQuery}
 * method, which evaluates templates as queries over graphs.
 *
 * <h2>Query solutions</h2>
 * The output of evaluating a template as a query over a graph in Reverse OTTR is a set of mappings.
 * An OTTR instance may be viewed as a mapping from the parameter variables of the template
 * it instantiates, to the arguments of the instance. Consider for example the following instances.
 * <pre>
 * Triple(john, hasName, "John") .
 * Triple(john, hasAge, 30) .
 * </pre>
 * Let {@code ?x}, {@code ?y} and {@code ?z} denote the first, second and third parameter of the Triple
 * template, respectively. Then, the instances above can be represented by the following mappings.
 * <pre>
 * {?x = john, ?y = hasName, ?z = "John"},
 * {?x = john, ?y = hasAge, ?z = 30}
 * </pre>
 * In this implementation, instances are
 * represented as mappings. As such, the output of the query process is a set of mappings
 * from variables to OTTR ground terms.
 * <p>
 * The following example shows the output from evaluating a simple template as a query over
 * an RDF graph in Reverse OTTR.
 * <p>
 * Template:
 * <pre>
 * Person[ ?person, ?name, ?age ] :: {
 *     Triple(?person, hasName, ?name),
 *     Triple(?person, hasAge, ?age)
 * } .
 * </pre>
 * Input graph (in RDF Turtle):
 * <pre>
 * john hasName "John" .
 * john hasAge 30 .
 * jane hasName "Jane" .
 * jane hasAge 31 .
 * jill hasName "Jill" .
 * </pre>
 * The result of querying the {@code Person} template over the input graph in Reverse OTTR
 * is a set of mappings from variables to ground terms, as follows.
 * <pre>
 * {?person = john, ?name = "John" ?age = 30},
 * {?person = jane, ?name = "Jane" ?age = 31}
 * </pre>
 * The mappings above correspond to the following instances.
 * <pre>
 * Person(john, "John", 30) .
 * Person(jane, "Jane", 31) .
 * </pre>
 * Notice that each mapping/instance in the solution expands, in forward OTTR, to a subset of the input graph,
 * which is the condition that the output must satisfy.
 *
 * <h2>Placeholders</h2>
 * In the example above, there are actually infinitely many instances of the Person template that
 * technically expands to subsets of the graph, for example, {@Code Person(john, "John", none)},
 * which expands to the empty graph.
 * In forward OTTR, instances with none as an argument for non-optional parameters are ignored
 * and are considered to produce the empty graph, which is a subset of all graphs.
 * Therefore, we should always generate every possible instance with none for some non-optional
 * parameter. However, there are infinitely many such instances in many cases: Every argument
 * that isn't none can be anything that doesn't produce an error.
 * Noticing that the other arguments for such instances doesn't affect the output in forward OTTR,
 * we instead use <strong>placeholders</strong> for the remaining arguments, in Reverse OTTR.
 * <p>
 * Placeholders represent infinite classes of ground terms (non-variable terms) and are used whenever
 * there's an infinite number of possible solutions. Therefore, for the example above, the full answer
 * consists of the following mappings, where {@code any} is a placeholder that represents all ground terms.
 * <pre>
 * {?person = john, ?name = "John" ?age = 30},
 * {?person = jane, ?name = "Jane" ?age = 31}
 * {?person = none, ?name = any ?age = any}
 * {?person = any, ?name = none ?age = any}
 * {?person = any, ?name = any ?age = none}
 * </pre>
 * <h2>Parameter modifiers</h2>
 * <h3>Optional/non-optional parameters</h3>
 * Concretely, in forward OTTR, optional parameters treat {@code none} as any other value.
 * In reverse, the optional parameter modifier retains this behavior, essentially doing nothing.
 * In the case of non-optional parameters, since instances with {@code none} as argument for
 * non-optional parameters are ignored, in reverse, we must generate all ignored solutions, as discussed in
 * the placeholder section above. The non-optional parameter also works as a filter on placeholders
 * restricting placeholders in potential solutions to exclude {@code none}.
 *
 * <h3>Non-blank parameters</h3>
 * Non-blank parameters work as filters in Reverse OTTR. No mapping in a query solution can
 * be blank for a non-blank parameter.
 *
 * <h3>Default values</h3>
 * For parameters with default values, arguments that are either {@code none} or the default value itself will expand
 * to the same graph. Therefore, in reverse, if a solution says that the parameter should map to its default
 * value, then we must also include a solution where the parameter maps to {@code none}, and is otherwise equal.
 * The implementation generates all such solutions.
 *
 * <h3>Types</h3>
 * Types are not dealt with in Reverse OTTR; they're simply ignored.
 *
 * <h2>List expansion</h2>
 * In forward OTTR, an instance with list expansion expands to multiple instances where
 * the arguments are elements of the lists in the input instance. Therefore, Reverse OTTR
 * finds the instances which could be subjected to list expansion to produce the input set
 * of instances. The ordering of the lists doesn't matter for list expansion in forward OTTR
 * so in reverse, we must consider all possible orders. This leads to a rapidly growing number
 * of solutions for reverse list expansion, relative to the size of generated lists.
 * The following example shows how list expansion works in reverse for expansion mode zipMin.
 * <p>
 * Say we are given the following instances.
 * <pre>
 * T(a1, b, c1) .
 * T(a2, b, c2) .
 * T(a3, b, c3) .
 * </pre>
 * Assume that we know, from the pattern of the template we are using as a query, that the first
 * and third arguments of the instances should be "unexpanded" to lists according to the
 * expansion mode zipMin.
 * Then we get the following solution.
 * <pre>
 * zipMin T(++(a1, a2, a3), b, ++(c1, c2, c3, any_trail)) .
 * </pre>
 * Here, {@code any_trail} is a placeholder which represents an infinite sequence of any ground terms.
 * Notice that {@code any_trail} would be ignored in forward OTTR, because zipMin only keeps
 * elements with index less or equal to the length of the shortest list. The placeholder {@code any_trail}
 * only occurs as the last element of lists and is only generated by reverse zipMin.
 * Notice also that forward expansion of the solution above leads to the exact same instances as the input.
 * <p>
 * As previously mentioned we must find all instances which expand to the input in forward OTTR.
 * Therefore, we must also include all solutions with different orderings of lists, as well as
 * sublists and also varying which list ends with {@code any_trail}.
 * The following are therefore also solutions.
 * <pre>
 * zipMin T(++(a2, a1, a3), b, ++(c2, c1, c3, any_trail)) .
 * zipMin T(++(a1, a2, any_trail), b, ++(c1, c2)) .
 * etc.
 * </pre>
 * Reverse list expansion is implemented in
 * {@link xyz.ottr.lutra.revottr.evaluation.ListUnexpander ListUnexpander}.
 *
 * <h2>Static blank nodes and lists</h2>
 * Templates with static blank nodes or lists are not evaluated correctly. The handling of
 * static blank nodes and lists in reverse is complicated due to their renaming during the
 * OTTR expansion process.
 */
package xyz.ottr.lutra.revottr;
/*-
 * #%L
 * xyz.ottr.lutra:lutra-revottr
 * %%
 * Copyright (C) 2018 - 2023 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */
