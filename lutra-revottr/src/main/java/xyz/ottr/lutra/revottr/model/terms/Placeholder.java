package xyz.ottr.lutra.revottr.model.terms;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-revottr
 * %%
 * Copyright (C) 2018 - 2023 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.Optional;
import java.util.Set;
import org.apache.jena.shared.PrefixMapping;
import xyz.ottr.lutra.OTTR;
import xyz.ottr.lutra.model.Parameter;
import xyz.ottr.lutra.model.terms.AbstractTerm;
import xyz.ottr.lutra.model.terms.Term;
import xyz.ottr.lutra.model.types.TypeRegistry;

/**
 * Represents a placeholder term which is intended to be used when the specific value
 * of a term in a result mapping is unknowable.
 * <p>
 * Due to parameter modifiers, we sometimes know which ground terms a placeholder
 * CANNOT represent. For example, the non-blank parameter modifier may let us
 * know that a placeholder cannot be a blank node. This may affect the possible
 * solutions for a Reverse OTTR query, in some cases.
 */
public class Placeholder extends AbstractTerm<String> {

    public static final String namespace = OTTR.namespace;

    /**
     * Represents all ground terms.
     */
    public static final Term any = new Placeholder(namespace + "any");

    /**
     * Represents all ground terms except for blank nodes (BlankTerm).
     */
    public static final Term anyNonBlank = new Placeholder(namespace + "any-non-blank");

    /**
     * Represents all ground terms except for none (NoneTerm).
     */
    public static final Term anyNonOptional = new Placeholder(namespace + "any-non-optional");

    /**
     * Represents all ground terms except for blank nodes and none.
     */
    public static final Term anyNonBlankNonOptional =
            new Placeholder(namespace + "any-non-blank-non-optional");

    /**
     * Represents a sequence of terms of arbitrary length (possibly zero).
     * It is assumed that any_trail only occurs as the last element of lists.
     * It is only used in the reverse of zipMin.
     * @see xyz.ottr.lutra.revottr.evaluation.ListUnexpander#unzipMin(Set)
     */
    public static final Term any_trail = new Placeholder(namespace + "any-trail");

    public Placeholder(String identifier) {
        super(identifier, TypeRegistry.LUB_TOP);
    }

    @Override
    public Optional<Term> unify(Term term) {
        return Optional.of(term);
    }

    @Override
    public Term shallowClone() {
        return new Placeholder(getIdentifier());
    }

    public String toString(PrefixMapping prefixes) {
        return prefixes.shortForm(getIdentifier());
    }

    public String toString() {
        return toString(OTTR.getDefaultPrefixes());
    }

    /**
     * Finds the placeholder which corresponds to the parameter modifiers of the
     * given parameter. This placeholder will then be used to filter all placeholders
     * that are mapped from the variable of this parameter in the mappings that result
     * from evaluating a template.
     * @param parameter A parameter
     * @return The placeholder that corresponds to the modifiers of the parameters
     */
    public static Term getCorresponding(Parameter parameter) {
        if (parameter.isNonBlank() && !parameter.isOptional()) {
            return anyNonBlankNonOptional;
        } else if (parameter.isNonBlank()) {
            return anyNonBlank;
        } else if (!parameter.isOptional()) {
            return anyNonOptional;
        } else {
            return any;
        }
    }

}
