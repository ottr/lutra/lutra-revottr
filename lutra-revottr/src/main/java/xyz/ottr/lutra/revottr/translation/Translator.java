package xyz.ottr.lutra.revottr.translation;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-revottr
 * %%
 * Copyright (C) 2018 - 2023 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import xyz.ottr.lutra.TemplateManager;
import xyz.ottr.lutra.model.Template;
import xyz.ottr.lutra.revottr.translation.model.SelectQuery;
import xyz.ottr.lutra.store.Expander;

/**
 * Translates templates into SPARQL select queries. Specifically, it translates templates
 * such that the resulting SPARQL select query produces roughly the same output as
 * the original template, evaluated in Reverse OTTR over some RDF graph. See the
 * {@link Parser#isTranslatable Parser.isTranslatable} method documentation for more information.
 * <p>
 * Simply put, a template is translatable if it has no default values, no list expansion
 * and, when evaluated as a revottr query, does not produce placeholder mappings that depend
 * on the graph its queried over. Again, see the
 * {@link Parser#isTranslatable(Template) Parser.isTranslatable} method documentation for a more detailed
 * explanation on which templates are translatable.
 */
public class Translator {

    private TemplateManager templateManager;
    private Expander expander;

    public Translator(TemplateManager templateManager, Expander expander) {
        this.templateManager = templateManager;
        this.expander = expander;
    }

    /**
     * Translates a template into a SPARQL select query, assuming it's translatable.
     * See {@link Parser#isTranslatable(Template)} for more information about which
     * templates are translatable.
     * <p>
     * For simplicity, the implementation expands templates before translating.
     * It is assumed that expanded templates produces the same results as unexpanded templates
     * when evaluated as Reverse OTTR queries, and expanding before translating simplifies the
     * parsing process.
     *
     * @param templateIRI The IRI of a template
     * @return An abstract representation of a SPARQL select query
     */
    public SelectQuery translate(String templateIRI) {
        Template template = templateManager.getTemplateStore().getTemplate(templateIRI).get();
        Template expanded = expander.expandTemplate(template).get();
        return Parser.parseFlat(expanded, templateManager.getPrefixes());
    }

    /**
     * Calls {@link Parser#isTranslatable(Template)} on the input template and if it is
     * translatable, translates it into a SPARQL select query.
     * <p>
     * For simplicity, the implementation expands templates before translating.
     * It is assumed that expanded templates produces the same results as unexpanded templates
     * when evaluated as Reverse OTTR queries, and expanding before translating simplifies the
     * parsing process.
     *
     * @param templateIRI The IRI of a template
     * @return An abstract representation of a SPARQL select query if it is translatable,
     * null otherwise
     */
    public SelectQuery checkAndTranslate(String templateIRI) {
        Template template = templateManager.getTemplateStore().getTemplate(templateIRI).get();
        Template expanded = expander.expandTemplate(template).get();
        if (Parser.isTranslatable(expanded)) {
            return Parser.parseFlat(expanded, templateManager.getPrefixes());
        }

        return null;
    }
}
