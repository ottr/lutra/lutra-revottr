package xyz.ottr.lutra.revottr.translation;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-revottr
 * %%
 * Copyright (C) 2018 - 2023 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.jena.shared.PrefixMapping;
import xyz.ottr.lutra.model.Argument;
import xyz.ottr.lutra.model.Instance;
import xyz.ottr.lutra.model.Parameter;
import xyz.ottr.lutra.model.Template;
import xyz.ottr.lutra.model.terms.Term;
import xyz.ottr.lutra.revottr.translation.model.SelectQuery;
import xyz.ottr.lutra.revottr.translation.model.condition.Condition;
import xyz.ottr.lutra.revottr.translation.model.condition.ConjunctiveCondition;
import xyz.ottr.lutra.revottr.translation.model.condition.EmptyCondition;
import xyz.ottr.lutra.revottr.translation.model.condition.NonBlank;
import xyz.ottr.lutra.revottr.translation.model.condition.NonOptional;
import xyz.ottr.lutra.revottr.translation.model.pattern.ConjunctivePattern;
import xyz.ottr.lutra.revottr.translation.model.pattern.EmptyPattern;
import xyz.ottr.lutra.revottr.translation.model.pattern.FilterPattern;
import xyz.ottr.lutra.revottr.translation.model.pattern.OptionalPattern;
import xyz.ottr.lutra.revottr.translation.model.pattern.Pattern;
import xyz.ottr.lutra.revottr.translation.model.pattern.TriplePattern;
import xyz.ottr.lutra.revottr.translation.model.terms.QTerm;
import xyz.ottr.lutra.revottr.translation.model.terms.Resource;
import xyz.ottr.lutra.revottr.translation.model.terms.Variable;

/**
 * A parser that constructs abstract SPARQL queries from templates.
 */
public class Parser {

    /**
     * Parses a template where every instance in its body is a base instance
     * and constructs an abstract representation of a SPARQL select query.
     *
     * @param template A template with only base instances in its body
     * @return An abstract representation of a SPARQL select query,
     * if translatable and null otherwise
     */
    public static SelectQuery parseFlat(Template template, PrefixMapping prefixMapping) {
        Pattern unfilteredPattern = parseUnfilteredPattern(template);
        List<Parameter> parameters = template.getParameters();
        Condition condition = parseCondition(parameters);
        Pattern pattern = new FilterPattern(unfilteredPattern, condition);

        List<Variable> resultVariables = parameters.stream()
                .map(Parameter::getTerm)
                .map(Term::getIdentifier)
                .map(Object::toString)
                .map(Variable::new)
                .collect(Collectors.toList());

        return new SelectQuery(prefixMapping, resultVariables, pattern);
    }

    /**
     * Creates a conjunctive pattern of triples based on the template pattern.
     * Instances in the template pattern that have no optional variables make up
     * the non-optional part of the resulting pattern, while the rest make up
     * the optional part.
     */
    private static Pattern parseUnfilteredPattern(Template template) {
        List<Parameter> parameters = template.getParameters();

        Pattern nonOptionalPattern = template.getPattern().stream()
                .filter(instance -> !isOptionalInstance(instance, parameters))
                .map(Parser::instanceToTriple)
                .reduce(ConjunctivePattern::new)
                .orElse(new EmptyPattern());

        Pattern optionalPattern = template.getPattern().stream()
                .filter(instance -> isOptionalInstance(instance, parameters))
                .map(Parser::instanceToTriple)
                .reduce(ConjunctivePattern::new)
                .orElse(new EmptyPattern());

        return new OptionalPattern(nonOptionalPattern, optionalPattern);
    }

    /**
     * Creates a sparql boolean expression to be used in the filter of the final query.
     * Non-blank parameters can not be blank in the final query and non-optional
     * parameters can not be equal to the IRI ottr:none
     */
    private static Condition parseCondition(List<Parameter> parameters) {
        Condition nonBlanks = parameters.stream()
                .filter(Parameter::isNonBlank)
                .map(Parameter::getTerm)
                .map(Term::getIdentifier)
                .map(Object::toString)
                .map(Variable::new)
                .map(NonBlank::new)
                .map(c -> (Condition) c)
                .reduce(ConjunctiveCondition::new)
                .orElse(new EmptyCondition());

        Condition nonOptionals = parameters.stream()
                .filter(parameter -> !parameter.isOptional())
                .map(Parameter::getTerm)
                .map(Term::getIdentifier)
                .map(Object::toString)
                .map(Variable::new)
                .map(NonOptional::new)
                .map(c -> (Condition) c)
                .reduce(ConjunctiveCondition::new)
                .orElse(new EmptyCondition());

        return new ConjunctiveCondition(nonBlanks, nonOptionals);
    }

    /**
     * Checks whether a template is translatable.
     * <p>
     * Given a template T, its SPARQL translation is a select query Q, such that
     * evaluating Q over a graph G produces roughly the same result as evaluating
     * T as a revottr query over G. In particular, by replacing missing values
     * (caused by optional patterns in Q) with the IRI ottr:none in the result of Q over G,
     * and adding certain placeholder mappings that can be statically determined
     * based on the parameters in T, we should achieve the exact same result as
     * evaluating T as a revottr query over G.
     * <p>
     * Intuitively, a template is translatable if the corresponding revottr query
     * does not produce placeholder results that partially depends on the graph
     * it's queried over. This method checks if the input template falls into a
     * general pattern that satisfies this condition:
     * <p>
     * Let a <strong>non-optional instance</strong> be an instance where every variable in its
     * argument list is a non-optional variable. Let two variables x and y <strong>depend</strong>
     * on each other in a template T if there is an instance I in the pattern of T
     * such that both x and y occur in the argument list of I.
     * Then, if the input template has been expanded all the way, such that it
     * only contains base instances in its pattern, then every template variable
     * must depend on a variable that occurs in a non-optional instance. Note that
     * by definition every variable depends on itself, so every variable that
     * occurs in the argument list of a non-optional instance satisfies this condition.
     * <p>
     * Additionally, templates with default values or list expansion modes are not
     * translatable.
     * <p>
     * Translatable template example:
     * <pre>
     * T[ ?x, ? ?y ] :: {
     *     Triple(?x, p1, o1),
     *     Triple(?x, p2, ?y)
     * } .
     * </pre>
     * Notice that ?y depends on ?x and ?x occurs in a non-optional instance.
     * This template will never produce placeholder mappings that depend on the graph
     * when evaluated as a Reverse OTTR query.
     * <p>
     * Non-translatable template example:
     * <pre>
     * T[ ?x, ? ?y, ?z ] :: {
     *     Triple(?x, p1, o1),
     *     Triple(?y, p2, ?z)
     * } .
     * </pre>
     * Notice that ?z depends only on ?y, but ?y does not occur as the argument of
     * a non-optional instance. This template may produce placeholder mappings
     * that depend on the graph.
     *
     * @param template A template where each instance is a base template
     * @return True, if the input template is translatable, false otherwise
     */
    public static boolean isTranslatable(Template template) {
        for (Parameter parameter : template.getParameters()) {
            if (parameter.hasDefaultValue()) {
                return false;
            }
        }

        for (Instance instance : template.getPattern()) {
            if (instance.hasListExpander()) {
                return false;
            }
        }

        Set<Instance> nonOptInstances = template.getPattern().stream()
                .filter(instance -> !isOptionalInstance(instance, template.getParameters()))
                .collect(Collectors.toSet());

        for (Instance instance : template.getPattern()) {
            if (!existsDependence(instance, nonOptInstances)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Checks whether an instance has a common variable with some instance in the
     * input set of instances
     */
    private static boolean existsDependence(Instance instance, Set<Instance> instances) {
        for (Instance other : instances) {
            if (depends(instance, other)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks whether two instances have a common variable in their argument lists
     */
    private static boolean depends(Instance i1, Instance i2) {
        for (Argument a1 : i1.getArguments()) {
            Term term = a1.getTerm();
            if (term.isVariable()) {
                for (Argument a2 : i2.getArguments()) {
                    if (term.equals(a2.getTerm())) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private static boolean isOptionalInstance(Instance instance, List<Parameter> parameters) {
        for (Argument argument : instance.getArguments()) {
            Term term = argument.getTerm();
            for (Parameter parameter : parameters) {
                Term variable = parameter.getTerm();
                if (term.equals(variable) && parameter.isOptional()) {
                    return true;
                }
            }
        }

        return false;
    }

    private static Pattern instanceToTriple(Instance instance) {
        List<QTerm> terms = instance.getArguments().stream()
                .map(Argument::getTerm)
                .map(Parser::termToQTerm)
                .collect(Collectors.toList());

        return new TriplePattern(terms);
    }

    private static QTerm termToQTerm(Term term) {
        if (term.isVariable()) {
            return new Variable(term.getIdentifier().toString());
        }

        return new Resource(term.getIdentifier().toString());
    }
}
