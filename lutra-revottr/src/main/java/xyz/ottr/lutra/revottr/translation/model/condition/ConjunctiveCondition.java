package xyz.ottr.lutra.revottr.translation.model.condition;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-revottr
 * %%
 * Copyright (C) 2018 - 2023 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import org.apache.jena.shared.PrefixMapping;

public class ConjunctiveCondition extends Condition {

    private Condition c1;
    private Condition c2;

    public ConjunctiveCondition(Condition c1, Condition c2) {
        this.c1 = c1;
        this.c2 = c2;
    }

    public String toString(PrefixMapping prefixMapping) {
        if (c1 instanceof EmptyCondition) {
            return c2.toString(prefixMapping);
        }

        if (c2 instanceof EmptyCondition) {
            return c1.toString(prefixMapping);
        }

        return c1.toString(prefixMapping) + " && " + c2.toString(prefixMapping);
    }
}
