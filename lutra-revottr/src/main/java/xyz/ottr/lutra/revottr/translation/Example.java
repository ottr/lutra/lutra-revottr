package xyz.ottr.lutra.revottr.translation;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-revottr
 * %%
 * Copyright (C) 2018 - 2023 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Set;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import xyz.ottr.lutra.TemplateManager;
import xyz.ottr.lutra.api.StandardFormat;
import xyz.ottr.lutra.api.StandardTemplateManager;
import xyz.ottr.lutra.revottr.evaluation.Evaluator;
import xyz.ottr.lutra.revottr.model.Mapping;
import xyz.ottr.lutra.revottr.translation.model.SelectQuery;
import xyz.ottr.lutra.store.Expander;
import xyz.ottr.lutra.store.expansion.CheckingExpander;

/**
 * Example program showcasing translation of template into SPARQL select query
 * and executing the query over an RDF graph.
 */
public class Example {

    public static void main(String[] args) {

        Path root = Paths.get("lutra-revottr", "src", "main", "resources");

        // Path to template library
        String libPath = root.resolve("exampleLib.stottr")
                .toAbsolutePath().toString();

        // Create template manager and read template lib
        TemplateManager templateManager = new StandardTemplateManager();
        templateManager.readLibrary(StandardFormat.stottr.name(), Collections.singleton(libPath));

        // Create a template expander from template store of previously read template lib
        Expander expander = new CheckingExpander(templateManager.getTemplateStore());

        // Translator for translating read templates into SPARQL select queries
        Translator translator = new Translator(templateManager, expander);

        String queryIRI = "http://example.org/Person";

        // Translate the template into abstract SPARQL select query, if it is translatable
        SelectQuery query = translator.checkAndTranslate(queryIRI);

        // Get string representation of query in SPARQL syntax
        String sparqlQueryString = query.toString();

        System.out.println("SPARQL query:");
        System.out.println(sparqlQueryString + "\n");

        // Parse SPARQL string into jena query for potential querying of graphs
        Query jenaQuery = QueryFactory.create(sparqlQueryString);

        // RDF Turtle graph path
        String graphPath = root.resolve("exampleGraph.ttl").toAbsolutePath().toString();

        // Read graph
        Model model = ModelFactory.createDefaultModel().read(graphPath);

        // Create execution and evaluate select query over graph
        try (QueryExecution execution = QueryExecutionFactory.create(jenaQuery, model)) {
            ResultSet result = execution.execSelect();

            // Print results
            System.out.println("SPARQL results:");
            result.forEachRemaining(System.out::println);
        }

        // Corresponding RevOTTR query
        Evaluator evaluator = new Evaluator(model, templateManager, 0);
        Set<Mapping> revottrResult = evaluator.evaluateQuery(queryIRI);
        System.out.println("\nRevOTTR results:");
        revottrResult.forEach(
                m -> System.out.println(m.toString(templateManager.getPrefixes())));
    }
}
