package xyz.ottr.lutra.revottr.model;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-revottr
 * %%
 * Copyright (C) 2018 - 2023 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.jena.shared.PrefixMapping;
import xyz.ottr.lutra.model.Parameter;
import xyz.ottr.lutra.model.terms.BlankNodeTerm;
import xyz.ottr.lutra.model.terms.ListTerm;
import xyz.ottr.lutra.model.terms.LiteralTerm;
import xyz.ottr.lutra.model.terms.NoneTerm;
import xyz.ottr.lutra.model.terms.Term;

/**
 * Represents an OTTR instance. A set of Mapping objects constitute the output of a Reverse OTTR query.
 * This class also implements several static helper methods for dealing with mappings in the context
 * of Reverse OTTR.
 */
public class Mapping {

    private final Map<Term, Term> map;

    /**
     * Creates an empty mapping object
     */
    public Mapping() {
        this.map = new HashMap<>();
    }

    /**
     * Creates a mapping from a variable to a term
     * @param var The initial variable of the mapping, represented as a term with the
     *            variable flag set
     * @param term The term to map the variable to
     */
    public Mapping(Term var, Term term) {
        this.map = new HashMap<>();
        this.put(var, term);
    }

    /**
     * Creates a mapping from a list of variables to a list of terms, pairwise by index
     * @param vars The initial variables of the mapping
     * @param terms The terms to map the variables to
     */
    public Mapping(List<Term> vars, List<Term> terms) {
        this.map = new HashMap<>();
        this.put(vars, terms);
    }

    /**
     * Adds a variable/term pair to the mapping
     * @param var A variable, represented as a term with the variable flag set
     * @param value The term to map the variable to
     */
    public void put(Term var, Term value) {
        this.map.put(var, value);
    }

    /**
     * Adds a list of variables and terms to the mapping, pairwise by index
     * @param vars A list of variables
     * @param terms The terms to map the variables to
     */
    public void put(List<Term> vars, List<Term> terms) {
        for (int i = 0; i < vars.size(); i++) {
            this.put(vars.get(i), terms.get(i));
        }
    }

    /**
     * Get the term mapped to by the given variable
     * @param var A variable, assumed to be in the domain of the mapping
     * @return The term mapped to by the given variable
     */
    public Term get(Term var) {
        return this.map.get(var);
    }

    /**
     * Gets a list of terms mapped to by the list of variables, in the same order as
     * the given variables
     * @param vars A list of variables, assumed to be in the mapping's domain
     * @return A list of terms mapped to by the input variables,
     * in the same order as the variable list
     */
    public List<Term> get(List<Term> vars) {
        List<Term> list = new LinkedList<>();

        for (Term var : vars) {
            list.add(this.get(var));
        }

        return list;
    }

    /**
     * Gets the domain of the mapping, i.e., the set of variables that are mapped to terms
     * @return The set of variables that are mapped to terms by the mapping
     */
    public Set<Term> domain() {
        return this.map.keySet();
    }

    /**
     * Checks if the mapping contains the given variable in its domain
     * @param var A variable
     * @return true, if the variable is in the mapping's domain, false, otherwise
     */
    public boolean containsVar(Term var) {
        return this.map.containsKey(var);
    }

    /**
     * Checks whether a mapping conforms to a list of parameters,
     * where the mapping-variables are the same as the parameter-variables
     */
    public boolean conforms(List<Parameter> parameters) {
        for (Parameter param : parameters) {
            Term var = param.getTerm();
            if (containsVar(var)) {
                boolean optTest = map.get(var) instanceof NoneTerm && !param.isOptional();
                boolean blankTest = param.isNonBlank() && map.get(var) instanceof BlankNodeTerm;
                if (optTest || blankTest) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Finds all default alternatives of this mapping.
     * A default alternative of a mapping is any mapping where variables, with default values
     * according to a parameter list, have a default value and the mapping maps the variable
     * to the default value or to the none-value.
     * In forward OTTR, all default alternatives of a mapping expands to the same graph, so
     * in reverse we must generate them all.
     */
    public Set<Mapping> defaultAlternatives(List<Parameter> parameters) {
        // The part of the mapping that stays the same for all default alternatives
        // of the given mapping
        Mapping baseMap = new Mapping();

        // The part of the mapping where the range may be either default values or none
        Mapping subMap = new Mapping();

        // Fills baseMap and subMap according to above comments
        for (Parameter param : parameters) {
            Term var = param.getTerm();
            if (param.hasDefaultValue()
                    && map.get(var).equals(param.getDefaultValue())) {
                subMap.put(var, map.get(var));

            } else {
                baseMap.put(var, map.get(var));
            }
        }

        // Return set of given mapping if it has no default alternatives
        if (subMap.domain().isEmpty()) {
            return Set.of(this);
        }

        // Compute default alternatives of subMap, combine with baseMap,
        // and return them as a set
        return Mappings.join(subMap.allCombinationsNone(), Set.of(baseMap));
    }

    /**
     * Finds all default alternatives of the input mapping, assuming
     * every term in its range can be replaced with none to produce
     * a default alternative
     */
    private Set<Mapping> allCombinationsNone() {
        Set<Set<Mapping>> combinations = new HashSet<>();

        for (Term var : domain()) {
            combinations.add(Set.of(
                    new Mapping(var, get(var)),
                    new Mapping(var, new NoneTerm())
            ));
        }

        return Mappings.joinAll(combinations);
    }

    @Override
    public String toString() {
        return this.map.toString();
    }

    /**
     * Pretty string representation of the mapping, utilizing the given prefixes
     * @param prefixes A Jena PrefixMapping of RDF Turtle prefixes
     * @return A string representation of the mapping
     */
    public String toString(PrefixMapping prefixes) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{");
        for (Term var : domain()) {
            stringBuilder.append("?");
            stringBuilder.append(var.getIdentifier());
            stringBuilder.append("=");

            Term term = get(var);
            String id;
            if (term instanceof ListTerm) {
                id = toString((ListTerm) term, prefixes);

            } else if (term instanceof LiteralTerm) {
                LiteralTerm literal = (LiteralTerm) term;
                id = prefixes.shortForm(literal.getValue());
                if (literal.isPlainLiteral()) {
                    id = "\"" + id + "\"";
                }

            } else {
                id = prefixes.shortForm(term.getIdentifier().toString());
            }

            stringBuilder.append(id);
            stringBuilder.append(", ");
        }
        stringBuilder.delete(stringBuilder.length() - 2, stringBuilder.length());
        stringBuilder.append("}");

        return stringBuilder.toString();
    }

    /**
     * Generates pretty string representation of ListTerms (possibly nested)
     */
    private String toString(ListTerm listTerm, PrefixMapping prefixes) {
        List<Term> list = listTerm.asList();

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");
        for (Term term : list) {
            String id;
            if (term instanceof ListTerm) {
                id = toString((ListTerm) term, prefixes);
            } else {
                id = prefixes.shortForm(term.getIdentifier().toString());
            }

            stringBuilder.append(id);
            stringBuilder.append(", ");
        }
        stringBuilder.delete(stringBuilder.length() - 2, stringBuilder.length());
        stringBuilder.append("]");
        stringBuilder.append("{").append(listTerm.getIdentifier()).append("}");

        return stringBuilder.toString();
    }

    /**
     * Checks for equality of mappings, treating NoneTerm objects as equal
     * @param o An object to compare with the Mapping object
     * @return True, if equal, false, otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Mapping mapping = (Mapping) o;

        if (!mapping.domain().equals(this.domain())) {
            return false;
        }

        for (Term var : this.domain()) {
            Term t1 = this.get(var);
            Term t2 = mapping.get(var);
            if (!t1.equals(t2) && !(t1 instanceof NoneTerm && t2 instanceof NoneTerm)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        for (Term var : domain()) {
            Term term = get(var);
            hash += term.hashCode();
        }

        return hash;
    }
}
