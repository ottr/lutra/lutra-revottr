package xyz.ottr.lutra.revottr.model;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-revottr
 * %%
 * Copyright (C) 2018 - 2023 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import xyz.ottr.lutra.model.terms.ListTerm;
import xyz.ottr.lutra.model.terms.Term;
import xyz.ottr.lutra.revottr.model.terms.Placeholder;

public class Mappings {
    /**
     * Takes the join of every set of mappings
     *
     * @param sets A set of sets of ground term mappings
     * @return A set of ground term mappings
     */
    public static Set<Mapping> joinAll(Set<Set<Mapping>> sets) {
        return sets.stream().reduce(Mappings::join).orElse(new HashSet<>());
    }

    /**
     * Takes the union of every mapping in the first set with every mapping in the second set,
     * if their union exists
     *
     * @param a A set of ground term mappings
     * @param b A set of ground term mappings
     * @return A set of ground term mappings
     */
    public static Set<Mapping> join(Set<Mapping> a, Set<Mapping> b) {
        Set<Mapping> resultSet = new HashSet<>();

        for (Mapping mapA : a) {
            for (Mapping mapB : b) {
                if (compatible(mapA, mapB)) {
                    resultSet.add(union(mapA, mapB));
                }
            }
        }

        return resultSet;
    }

    /**
     * Combines two mappings. For shared variables, the union maps to the PartialOrder of what
     * the two mappings map to
     *
     * @param a A ground term mapping
     * @param b A ground term mapping
     * @return The union of the two given mappings
     */

    // TODO MGS: Implement this using Map.merge?
    public static Mapping union(Mapping a, Mapping b) {

        Mapping result = new Mapping();
        Set<Term> domain = new HashSet<Term>();
        domain.addAll(a.domain());
        domain.addAll(b.domain());

        for (Term var : domain) {
            if (!a.containsVar(var)) {
                result.put(var, b.get(var));
            } else if (!b.containsVar(var)) {
                result.put(var, a.get(var));
            } else {
                result.put(var, PartialOrder.getGLB(a.get(var), b.get(var)));
            }
        }

        return result;
    }

    /**
     * Two mappings are compatible if for each shared variable in their domain,
     * the PartialOrder exists for the terms they map to
     *
     * @param a A ground term mapping
     * @param b A ground term mapping
     * @return True, if the mappings are compatible, false, otherwise
     */
    public static boolean compatible(Mapping a, Mapping b) {
        for (Term var : a.domain()) {
            if (b.containsVar(var) && PartialOrder.getGLB(a.get(var), b.get(var)) == null) {
                return false;
            }
        }
        return true;
    }

    /**
     * Creates a mapping from the variables in the range of argMap to the
     * terms in the range of termMap. It's assumed that argMap and termMap
     * have the same domain.
     *
     * @param termMap Mapping from template variables to ground terms.
     *                Can be thought of as representing a ground instance.
     * @param argMap  Mapping from template variables to terms.
     *                Can be thought of as representing an instance with variable arguments.
     * @return Mapping from variables in range of argMap to ground terms in range of termMap.
     */
    public static Mapping transform(Mapping termMap, Mapping argMap) {
        Mapping result = new Mapping();
        for (Term var : argMap.domain()) {
            Term argValue = argMap.get(var);
            Term termValue = termMap.get(var);
            Mapping tempMap = termTransform(termValue, argValue);
            if (compatible(result, tempMap)) {
                result = union(result, tempMap);
            }
        }

        return result;
    }

    private static Mapping termTransform(Term term, Term arg) {
        Mapping result = new Mapping();

        if (arg instanceof ListTerm && term instanceof ListTerm) {
            return listTransform(((ListTerm) term).asList(), ((ListTerm) arg).asList());

        } else if (arg.isVariable()) {
            result.put(arg, term);
        }

        return result;
    }

    private static Mapping listTransform(List<Term> termList, List<Term> argList) {
        int size = argList.size();

        if (termList.get(termList.size() - 1).equals(Placeholder.any_trail)) {
            size = termList.size() - 1;
        }

        Mapping result = new Mapping();

        for (int i = 0; i < size; i++) {
            Term arg = argList.get(i);
            Term term = termList.get(i);

            Mapping tempMap = termTransform(term, arg);

            if (compatible(result, tempMap)) {
                result = union(result, tempMap);
            }
        }

        for (int i = size; i < argList.size(); i++) {
            Term arg = argList.get(i);
            if (arg.isVariable()) {
                Mapping tempMap = new Mapping();
                tempMap.put(arg, Placeholder.any);
                result = union(result, tempMap);
            }
        }

        return result;
    }

    /**
     * Used to check if a mapping (termMap) fits the argument list of an instance
     * (represented by argMap).
     * It's assumed that argMap and termMap have the same domain.
     *
     * @param termMap A mapping representing a ground instance
     * @param argMap  A mapping representing an instance (possibly with variables
     *                in the argument list)
     * @return True, if for each variable in their domain, the terms they map to
     * are compatible or the term in argMap is a variable
     */
    public static boolean innerCompatible(Mapping termMap, Mapping argMap) {
        if (!termMap.domain().equals(argMap.domain())) {
            return false;
        }

        // TODO: deal with variables in lists
        for (Term var : argMap.domain()) {
            Term arg = argMap.get(var);
            Term term = termMap.get(var);

            if (!arg.isVariable() && PartialOrder.getGLB(arg, term) == null) {
                return false;
            }
        }

        return true;
    }
}
