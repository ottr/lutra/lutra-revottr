package xyz.ottr.lutra.revottr.model;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-revottr
 * %%
 * Copyright (C) 2018 - 2023 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.LinkedList;
import java.util.List;
import xyz.ottr.lutra.model.terms.BlankNodeTerm;
import xyz.ottr.lutra.model.terms.IRITerm;
import xyz.ottr.lutra.model.terms.ListTerm;
import xyz.ottr.lutra.model.terms.LiteralTerm;
import xyz.ottr.lutra.model.terms.NoneTerm;
import xyz.ottr.lutra.model.terms.Term;
import xyz.ottr.lutra.revottr.model.terms.Placeholder;
import xyz.ottr.lutra.revottr.model.terms.RListTerm;

/**
 * Indirectly encodes a partial order on ground terms and placeholders. The partial order is
 * represented here by methods for comparing and combining ground terms and placeholders.
 * Placeholders represent infinite classes of ground terms and are used in query results when
 * we need a value, but the number of possible values is infinite, and we cannot narrow it down
 * to a concrete term.
 *
 * @see xyz.ottr.lutra.revottr.evaluation.Evaluator
 * @see Placeholder
 */
public enum PartialOrder {
    ;

    /**
     * Finds the greatest lower bound (GLB) of ground terms and placeholders. It is
     * used when result mappings are combined during query evaluation. For example
     * when the mappings produced by the instances in a template body are combined.
     * The GLB is needed due to placeholders that represent entire ranges of terms.
     *
     * @param t1 A ground term
     * @param t2 A ground term
     * @return A ground term that is the GLB of t1 and t2, or null if the GLB doesn't exist
     */
    public static Term getGLB(Term t1, Term t2) {
        if (isLessOrEqual(t1, t2)) {
            return t1;
        }

        if (isLessOrEqual(t2, t1)) {
            return t2;
        }

        if (t1 instanceof ListTerm && t2 instanceof ListTerm) {
            List<Term> list = getGLB(((ListTerm) t1).asList(), ((ListTerm) t2).asList());
            if (list != null) {
                return new RListTerm(list);
            }
        }

        if (t1.equals(Placeholder.anyNonBlank) && t2.equals(Placeholder.anyNonOptional)
                || t1.equals(Placeholder.anyNonOptional) && t2.equals(Placeholder.anyNonBlank)) {
            return Placeholder.anyNonBlankNonOptional;
        }

        return null;
    }

    /**
     * Computes the greatest lower bound (GLB) of two term lists. The GLB of two
     * lists is a new list where each element is the element-wise GLB of
     * the input lists. If one of the lists are "trailing", i.e., has
     * Placeholder.any_trail as their last element, then the remaining
     * elements of the other list are appended onto the result. This is
     * because, intuitively, a trailing list L represents the infinite class
     * of lists that are equal to L up until the trailing part, i.e., the
     * index of Placeholder.any_trail.
     */
    private static List<Term> getGLB(List<Term> l1, List<Term> l2) {

        List<Term> shorter = l1;
        List<Term> longer = l2;

        if (l1.size() > l2.size()) {
            shorter = l2;
            longer = l1;
        }

        List<Term> result = new LinkedList<>();

        for (int i = 0; i < shorter.size() - 1; i++) {
            Term glbTerm = getGLB(shorter.get(i), longer.get(i));
            if (glbTerm != null) {
                result.add(glbTerm);
            } else {
                return null;
            }
        }

        if (trailing(shorter)) {
            for (int i = shorter.size() - 1; i < longer.size(); i++) {
                result.add(longer.get(i));
            }
            return result;

        } else if (trailing(longer) && shorter.size() == longer.size()) {
            result.add(shorter.get(shorter.size() - 1));
            return result;

        } else if (trailing(longer) && shorter.size() == longer.size() - 1) {
            return result;

        } else {
            return null;
        }
    }

    /**
     * Compares ground terms, including placeholders.
     * Intuitively, placeholders represent infinite classes of ground terms and
     * are considered greater than all ground terms in the class they represent.
     *
     * @param t1 A ground term
     * @param t2 A ground term
     * @return True, if t1 is considered less or equal to t2, false, otherwise
     * @see Placeholder
     */
    public static boolean isLessOrEqual(Term t1, Term t2) {
        if (t1.equals(t2)) {
            return true;
        }

        if (t2.equals(Placeholder.any)) {
            return true;
        }

        if (t1.equals(Placeholder.anyNonBlankNonOptional)
                && (isLessOrEqual(Placeholder.anyNonBlank, t2)
                || isLessOrEqual(Placeholder.anyNonOptional, t2))) {
            return true;
        }

        if (t1 instanceof NoneTerm && t2 instanceof NoneTerm) {
            return true;
        }

        if (t1 instanceof NoneTerm && isLessOrEqual(Placeholder.anyNonBlank, t2)) {
            return true;
        }

        if (t1 instanceof BlankNodeTerm && isLessOrEqual(Placeholder.anyNonOptional, t2)) {
            return true;
        }

        if ((t1 instanceof IRITerm
                || t1 instanceof LiteralTerm
                || t1 instanceof ListTerm)
                && isLessOrEqual(Placeholder.anyNonBlankNonOptional, t2)) {
            return true;
        }

        if (t1 instanceof ListTerm && t2 instanceof ListTerm) {
            return isLessOrEqual((ListTerm) t1, (ListTerm) t2);
        }

        return false;
    }

    /**
     * A list l1 is considered less or equal to another list l2 if l1 is
     * element-wise less or equal to l2. If the last element of l2 is
     * any_trail, then l1 only has to be element-wise less than l2 up
     * to the index of any_trail in l2, exclusively.
     */
    private static boolean isLessOrEqual(ListTerm t1, ListTerm t2) {
        if (t1.getIdentifier().equals(t2.getIdentifier())) {
            return true;
        }

        if (!isUnexpanded(t2)) {
            return false;
        }

        List<Term> list1 = t1.asList();
        List<Term> list2 = t2.asList();

        if (list1.isEmpty() && list2.isEmpty()) {
            return true;
        }

        if (list1.size() < list2.size() - 1) {
            return false;
        }

        for (int i = 0; i < list2.size() - 1; i++) {
            if (!isLessOrEqual(list1.get(i), list2.get(i))) {
                return false;
            }
        }

        if (list1.size() == list2.size()
                && !isLessOrEqual(list1.get(list1.size() - 1), list2.get(list2.size() - 1))
                && !trailing(list2)) {
            return false;
        }

        return trailing(list2);
    }


    // TODO MGS move elsewhere?
    public static boolean trailing(List<Term> list) {
        return list.size() > 0 && list.get(list.size() - 1).equals(Placeholder.any_trail);
    }

    // TODO MGS move elsewhere?
    public static boolean isUnexpanded(Term t) {
        return t instanceof RListTerm;
    }
}
